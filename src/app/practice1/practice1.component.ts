import { Component } from '@angular/core';

//ques3:
interface IData {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
  ContactNo: string;
}

@Component({
  selector: 'app-practice1',
  templateUrl: './practice1.component.html',
  styleUrls: ['./practice1.component.scss']
})
export class Practice1Component {

  // constructor(){
  //   this.addData();
  //   console.log(this.kk);
  // }
  constructor(){
    this.addData();

    //ques4:
    console.log(this.kk);
  }
  public DataSet: IData[] = [
    {
      Id: 1,
      FirstName: "kamal",
      LastName: "swain",
      Email: "kamal@22gmail.com",
      ContactNo: "123456790"
    },
    {
      Id: 2,
      FirstName: "sajid",
      LastName: "ali",
      Email: "sajidali@gmail.com",
      ContactNo: "2345678919"
    },
    {
      Id: 3,
      FirstName: "prince",
      LastName: "ray",
      Email: "princeray.cybrain@gmail.com",
      ContactNo: "3456789312"
    }
  ];

  addData() {
    const newData: IData = {
      Id: 4,
      FirstName: "aditya",
      LastName: "raj",
      Email: "adityaraj@23devilgmail.com",
      ContactNo: "56789364412"
    };
    this.DataSet.push(newData);
  }

//ques4:
  kk=this.DataSet.push();
}  



