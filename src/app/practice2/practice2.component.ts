import { Component } from '@angular/core';


@Component({
  selector: 'app-practice2',
  templateUrl: './practice2.component.html',
  styleUrls: ['./practice2.component.scss']
})
export class Practice2Component {

  //ques8,10,13,14,15,20,16:
  constructor(){
    this.onit();
    this.c();
    this.pp();
    this.list1();
    this.twodarray();
    console.log( this.removedElements);
    console.log(this.copiedArray);
    this.fruit();
  }

  //ques7:
   array1 :string[]= ['kamal'];
   array2 :string[]= ['nayan'];
   addd:string[]=[...this.array1, ...this.array2];

   //ques8:
pp(){
  const pop=this.addd.pop();

}

//ques10:
myArray = [];
onit(){
  if (this.myArray.pop() === undefined) {
    console.log("The array is empty.");
  } else {
    console.log("The array is not empty.");
  }

}

//ques13:
 public common = ["apple", "banana", "cherry", "date", "elderberry","grapes","orange"];

c(){
  this.common.splice(2, 1);

}

//ques14:
list=[1,2,3,4,5,12,23,44]
list1(){

  this.list.splice(2, 2);
}
//ques18:
 originalArray = [1, 2, 3, 4, 5,6,7,8,9];

 copiedArray = this.originalArray.slice();

//ques15:
removedElements = this.originalArray.splice(1, 2);

//ques20:
 data = [
  [1, "papaya"],
  [3, "grapes"],
  [2, "watermelon"],
  [4, "day"]
];


twodarray(){
  const idArray = this.data.map(subArray => subArray[0]);
}


//ques16:
fruits:string[] = ['apple', 'banana', 'cherry','watermelon','orange','papaya'];
fruit(){
  
  this.fruits.splice(1, 1, 'kiwi');
  
  console.log(this.fruits);
}




}

//9 - What is the return value of the pop() function?
//sol:it will delete all the value at the end.

//11.Can the pop() function be used to remove elements from the beginning of an array?
//sol:no,If you want to remove an item from an array, you can use the pop() method to remove the last element or the shift() method to remove the first element.

//17.What is the difference between the slice() and splice() functions?
//sol:The slice() method can be used to create a copy of an array or return a portion of an array. It is important to note that the slice() method does not alter the original array but instead creates a shallow copy. Unlike the slice() method, the splice() method will change the contents of the original array.
