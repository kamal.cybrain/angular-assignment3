import { Component } from '@angular/core';
//ques 2:
interface nnnnn {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
  company:string;
  contact:number ;
}

@Component({
  selector: 'app-practice',
  templateUrl: './practice.component.html',
  styleUrls: ['./practice.component.scss']
})
export class PracticeComponent {
  constructor(){ 
    this.appdata();
    this.dataset();
  }
  
  //ques2:
  DataSet1: nnnnn[] = [
    { Id: 1, FirstName: "John", LastName: "Doe", Email: "john.doe@example.com" ,company:"xyz" ,contact:1234},
    { Id: 2, FirstName: "Jane", LastName: "Doe", Email: "jane.doe@example.com" ,company:"abc",contact:2222 },
    
  ];

  
  DataSet: { Id: number, FirstName: string, LastName: string, Email: string, company:string,contact:number }[] = [
    { Id: 1, FirstName: "kamal", LastName: "swain", Email: "kamal.cybrain@gmail.com", company:"cybrain", contact: 12346789 },
    { Id: 2, FirstName: "sajid", LastName: "ali", Email: "sajid.digit@gmail.com", company:"digit", contact:1222222 },
    
  ];
  
  //ques6:
  newData= {
    Id: 3, FirstName: "kamal", LastName: "swain", Email: "kamal.swain@gmail.com", company:"cybrain", contact: 12346789 }
    appdata(){
      
      if (!this.DataSet.some(data => data.Email === this.newData.Email)) {
        this.DataSet.push(this.newData);
        
      }
    }
    
    //ques19:
    dataset(){
      this.DataSet.sort((a, b) => b.Id - a.Id);
      console.log(this.DataSet);
      

  }
}

//5.Can the push() function be used to add elements to the beginning of an array?
//sol:When you want to add an element to the end of your array, use push() . If you need to add an element to the beginning of your array, use unshift() . If you want to add an element to a particular location of your array, use splice().
  
  
 
  


